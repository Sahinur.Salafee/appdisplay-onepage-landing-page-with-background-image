jQuery(document).ready(function ($) {
    "use strict";
    $(".carousel-inner .item:first-child").addClass("active");
    
    /* Scroll to top
    ===================*/
    $.scrollUp({
        scrollText: '<span class="ti-rocket"></span>',
        easingType: 'linear',
        scrollSpeed: 900,
        animation: 'fade'
    });

    /*WoW js Active
     =================*/
    new WOW().init({
        mobile: true,
    });
    /* list_screen_slide Active
    =============================*/
    // $('.bg-slide').owlCarousel({
    //     loop: true,
    //     nav: false,
    //     margin: 0,
    //     autoplay: true,
    //     autoplayTimeout: 4000,
    //     smartSpeed: 500,
    //     items: 1,
    //     animateIn: 'fadeIn',
    //     animateOut: 'fadeOut'
    // });
    /* list_screen_slide Active
    =============================*/
    $('.list_screen_slide').owlCarousel({
        loop: true,
        responsiveClass: true,
        nav: true,
        margin: 5,
        autoplay: true,
        autoplayTimeout: 4000,
        smartSpeed: 500,
        center: true,
        navText: ['<span class="ti-angle-left"></span>', '<span class="ti-angle-right"></span>'],
        responsive: {
            0: {
                items: 1,
            },
            600: {
                items: 3
            },
            1200: {
                items: 5
            }
        }
    });
    
    
    /* single_screen_slide Active
    =============================*/
    var single_screen_slide = $('.single_screen_slide');
    single_screen_slide.owlCarousel({
        loop: true,
        margin: 0,
        dots: true,
        autoplay: true,
        autoplayTimeout: 4000,
        smartSpeed: 1000,
        mouseDrag: true,
        touchDrag: false,
        center: true,
        items: 1,
    });
   
    /*--------------------
    MAGNIFIC POPUP JS
    ----------------------*/
    $('.work-popup').magnificPopup({
        type: 'image',
        removalDelay: 300,
        mainClass: 'mfp-with-zoom',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true, // By default it's false, so don't forget to enable it

            duration: 300, // duration of the effect, in milliseconds
            easing: 'ease-in-out', // CSS transition easing function

            // The "opener" function should return the element from which popup will be zoomed in
            // and to which popup will be scaled down
            // By defailt it looks for an image tag:
            opener: function (openerElement) {
                // openerElement is the element on which popup was initialized, in this case its <a> tag
                // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                return openerElement.is('img') ? openerElement : openerElement.find('img');
            }
        }
    });
    
    /* Instagram-jQuery */

     $('.feature-area a').on('mouseenter', function () {
        $(this).tab('show');
    });
}(jQuery));


