// Wow js

        new WOW().init();
// owl Carousel One


$('#owl-one').owlCarousel({
            animateOut: 'zoomOut',
   			animateIn: 'zoomIn',
		    loop:true,
		    autoplay:true,
			autoplayTimeout:2000,
			autoplayHoverPause:false,
		    margin:20,
		    nav:false,
		    dots:false,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:1
		        }
		    }
			});


// owl Carousel Two

$('#owl-two').owlCarousel({
    		animateOut: 'zoomOut',
   			animateIn: 'fadeInRight',
   			stagePadding:30,
    		smartSpeed:450,
        	loop:true,
        	autoplay:true,
        	dots:true,
        	margin:20,
		    nav:false,
		    items: 5,
		    responsive:{
		        0:{
		            items:1
		        },
		        600:{
		            items:1
		        },
		        1000:{
		            items:3
		        }
		    }
		});